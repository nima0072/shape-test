<?php

require_once "./db/DataBase.php";

class BaseClass
{
    private PDO $_db;

    public function __construct()
    {
        if (empty($this->_db)) {
            $mydb = new DataBase();
            $this->_db = $mydb->getDatabase();
        }
    }

    function add($mostatil)
    {

        $x = $mostatil["x"] + 0;
        $y = $mostatil["y"] + 0;
        $width = $mostatil["width"] + 0;
        $height = $mostatil["height"] + 0;
        $time = $mostatil["time"];

        $query = "INSERT INTO shape (x, y, width, height, time)
        VALUES ('$x', '$y', '$width', '$height', '$time')";

        echo $query;

        try
        {
            $this->_db->query($query);
        }
        catch(Exception $e)
        {
            echo $e;
        }
        
    }

    function get()
    {
        $query = "SELECT * FROM shape";
        $json = $this->_db->prepare($query);
        $json->execute();
        $results = $json->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($results);
    }


    function check_mostatils($main, $input)
    {
        function check_eshterak($obj1, $obj2)
        {
            $res = 0;

            $zel_rast_1 = $obj1["x"] + $obj1["width"];

            // check width
            if ($zel_rast_1 <= $obj2["x"] + $obj2["width"] && $zel_rast_1 >= $obj2["x"]) {
                $res++;
            }

            $zel_pain_1 = $obj1["y"] + $obj1["height"];

            // check height
            if ($zel_pain_1 >= $obj2["y"] && $zel_pain_1 <= $obj2["y"] + $obj2["height"]) {
                $res++;
            }
            return $res;
        }



        $res = [];

        foreach ($input as $obj) {
            $result = check_eshterak($obj, $main) + check_eshterak($main, $obj);
            if ($result >= 2) {
                $res[] = $obj;
            }
        }
        return $res;
    }
}
