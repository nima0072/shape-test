<?php

class DataBase
{
    private PDO $_db;

    public function __construct()
    {
        if(empty($this->_db))
        {
            $this->_db = new PDO("mysql:host=localhost;dbname=shapes", "root", "");
        }
    }

    public function getDatabase()
    {
        return $this->_db;
    }
    
}
