<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>project</title>
</head>
<script src="static/jquery-3.2.1.min.js"></script>
<?php



?>

<script>
    function load_data() {
        $.get({
            url: "/reqhandler.php",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                give_data: 1
            }),
            success: function(data) {
                load_list(data);
            },
        });
    }

    function list_loader(array, place, is_new_msg = 0) {
        if (array) {
            var user = get_users();
            array.forEach(e => {

                var htm = $("#data_example").clone();

                htm.show();

                htm.attr("id", e.id);

                $("#" + place).append(htm);

            });
        }
    }
</script>

</html>