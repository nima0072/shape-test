<?php

require_once "./classes/BaseClass.php";

function getJsonInput()
{
    $in = file_get_contents('php://input');
    if ($in) {
        return json_decode($in, true);
    }
    return array();
}

$post = getJsonInput();

if (!empty($post["main"])) 
{
    $db = new BaseClass();
    

    $valid_mostatils = $db->check_mostatils($post["main"], $post["input"]);
    
    if (!empty($valid_mostatils)) {

        foreach ($valid_mostatils as $mostatil) {
            $date = date("Y-m-d")." ".date("h:i:sa");
            $date = substr($date, 0, -2);
            $mostatil["time"] = strval($date);
            //print_r($mostatil);
            
            $db->add($mostatil);
        }
    }
} 
else {
    $db = new BaseClass();
    $data = $db->get();
    print_r($data);
}
